#!/bin/bash
# Run experimentations

cd
cd 3d-query-replace/builds/release

# 5.1 Hexahedral subdivision

./hexa-subdivision ../../data/mesh1.off -lmax 7
./hexa-subdivision ../../data/mesh2.off -lmax 7
./hexa-subdivision ../../data/mesh3.off -lmax 7

# 5.2 Marching-cube

./hexa-subdivision ../../data/mesh1.off -lmax 7 -marching-cubes
./hexa-subdivision ../../data/mesh2.off -lmax 7 -marching-cubes
./hexa-subdivision ../../data/mesh3.off -lmax 7 -marching-cubes

# 5.3 Conformal Mesh

./hexa-subdivision ../../data/mesh1.off -lmax 7 -create-transitions
./hexa-subdivision ../../data/mesh2.off -lmax 7 -create-transitions
./hexa-subdivision ../../data/mesh3.off -lmax 7 -create-transitions

# Comparison with version WITHOUT signature:

./hexa-subdivision ../../data/mesh1.off -lmax 7 -create-transitions -no-signature
./hexa-subdivision ../../data/mesh2.off -lmax 7 -create-transitions -no-signature
./hexa-subdivision ../../data/mesh3.off -lmax 7 -create-transitions -no-signature

# 5.4 Mesh Transformation

./tetra-to-hexa ../../data/mesh1.off
./tetra-to-hexa ../../data/mesh2.off
./tetra-to-hexa ../../data/mesh3.off

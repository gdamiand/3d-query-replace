#!/bin/bash
# Installation script for Ubuntu 22.04

# Install packages

sudo apt-get update
sudo apt-get install g++ cmake git
sudo apt-get install libqt5opengl5-dev libqt5svg5-dev qtbase5-dev qtscript5-dev
sudo apt-get install libboost-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev
sudo apt-get install libgmp-dev libmpfr-dev zlib1g-dev libtet1.5-dev libassimp-dev

# Download code

cd
git clone https://github.com/CGAL/cgal.git
cd cgal
git checkout 2b547ec58a9e34458792cb3f09ead399bb6dd075
cd ..
git clone https://gitlab.liris.cnrs.fr/gdamiand/3d-query-replace.git
cd 3d-query-replace/

# Compile the two programs ```hexa-subdivision``` and ```tetra-to-hexa```

cd
cd 3d-query-replace/
mkdir builds
cd builds
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=../../../cgal/ ../../src 
make

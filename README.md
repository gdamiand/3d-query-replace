# 3D-query-replace

Program for the paper Query-Replace Operations for Topologically Controlled 3D Mesh Editing. Guillaume Damiand, Vincent Nivolliers.

This repository repository contains code, scripts and dataset.

Let us denote by SRC_DIR the root directory of the gitlab repository.

# 1. Install Required Dependencies

   * [CGAL](https://www.cgal.org/), with component Qt5
      Tested with master version 2b547ec58a9e34458792cb3f09ead399bb6dd075
      (cgal master, May 17th 2022)
   * [Boost](https://www.boost.org/), with components system and filesystem
   * [Qt5](https://www.qt.io/)
   * [Assimp](https://github.com/assimp/assimp)
   * [TetGen](https://wias-berlin.de/software/index.jsp?id=TetGen&lang=1)

On Ubuntu 22.04, the following versions of these libraries are used:
   * Boost 1.74
   * Qt5 5.15
   * Assimp 5.2
   * TetGen 1.5

# 2. List of commands for Ubuntu 22.04

If you want to use docker:

   * docker pull ubuntu:22.04
   * docker run -it  ubuntu:22.04
   * (docker commit to save the state of a container)
   
Installation commands:

   * apt-get update
   * apt-get install g++ cmake git
   * apt-get install libqt5opengl5-dev libqt5svg5-dev qtbase5-dev qtscript5-dev
   * apt-get install libboost-dev libboost-program-options-dev libboost-system-dev libboost-thread-dev
   * apt-get install libgmp-dev libmpfr-dev zlib1g-dev libtet1.5-dev libassimp-dev
   * cd
   * git clone https://github.com/CGAL/cgal.git
   * cd cgal
   * git checkout 2b547ec58a9e34458792cb3f09ead399bb6dd075
      (cgal master, May 17th 2022)
   * cd ..
   * git clone https://gitlab.liris.cnrs.fr/gdamiand/3d-query-replace.git
   * cd 3d-query-replace/
   
# 3. Compile the Two Programs ```hexa-subdivision``` and ```tetra-to-hexa```

   *// We suppose the current directory is SRC_DIR*
   ```
   mkdir builds
   cd builds
   mkdir release
   cd release
   cmake -DCMAKE_BUILD_TYPE=Release ../../src -DCGAL_DIR=/root/cgal/
   make
   ```

   If necessary, replace ```/root/cgal``` by the full path to the directory containing CGAL.


# 4. Run the Programs

   One example of run:

   *// We suppose the current directory is SRC_DIR/builds/release*
   ```
   ./hexa-subdivision ../../data/mesh1.off -lmax 5 -draw
   ```

   * The first parameter is the filename (surface mesh to load)
   * -lmax gives the maximal subdivision level
   * -draw will draw the final 3-map

   Run ```./hexa-subdivision -h``` (or ```./tetra-to-hexa -h```) for an help showing all options. Press the ```h``` key to view the help of the interactive viewer.

# 5. Reproduce the Experiments of the Paper

   *// We suppose the current directory is SRC_DIR/builds/release*
   
   The experiments given in the paper are obtained by the following runs (for each run, use -draw option to view the final mesh):

   # 5.1 Hexahedral subdivision

   ```
   ./hexa-subdivision ../../data/mesh1.off -lmax 7
   ./hexa-subdivision ../../data/mesh2.off -lmax 7
   ./hexa-subdivision ../../data/mesh3.off -lmax 7
   ```

   # 5.2 Marching-cube

   ```
   ./hexa-subdivision ../../data/mesh1.off -lmax 7 -marching-cubes
   ./hexa-subdivision ../../data/mesh2.off -lmax 7 -marching-cubes
   ./hexa-subdivision ../../data/mesh3.off -lmax 7 -marching-cubes
   ```

   # 5.3 Conformal Mesh

   ```
   ./hexa-subdivision ../../data/mesh1.off -lmax 7 -create-transitions
   ./hexa-subdivision ../../data/mesh2.off -lmax 7 -create-transitions
   ./hexa-subdivision ../../data/mesh3.off -lmax 7 -create-transitions
   ```

   Comparison with version WITHOUT signature:

   ```
   ./hexa-subdivision ../../data/mesh1.off -lmax 7 -create-transitions -no-signature
   ./hexa-subdivision ../../data/mesh2.off -lmax 7 -create-transitions -no-signature
   ./hexa-subdivision ../../data/mesh3.off -lmax 7 -create-transitions -no-signature
   ```

   # 5.4 Mesh Transformation

   Require [TetGen](https://wias-berlin.de/software/index.jsp?id=TetGen&lang=1)

   ```
   ./tetra-to-hexa ../../data/mesh1.off
   ./tetra-to-hexa ../../data/mesh2.off
   ./tetra-to-hexa ../../data/mesh3.off
   ```
